import 'package:flutter_modular/flutter_modular.dart';
import 'package:logger/logger.dart';

class CoreModule extends Module {
  @override
  List<Bind> get binds => [
        Bind.singleton((i) => Logger(), export: true),
      ];
}
