import 'package:some_app/core/constants/runtime_values.dart';

extension Validator on String {
  bool isValidEmail() {
    return RegExp(RuntimeVal.emailRegex).hasMatch(this);
  }

  bool isValidPassword() {
    return RegExp(RuntimeVal.passwordRegex).hasMatch(this);
  }

  bool isValidUserName() {
    return RegExp(RuntimeVal.userNameRegex).hasMatch(this);
  }
}
