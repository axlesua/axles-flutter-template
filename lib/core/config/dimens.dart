class Dimens {
  static const double max = double.infinity;
}

class Radiuses {
  static const double tiny = 4.0;
  static const double small = 8.0;
  static const double medium = 12.0;
  static const double big = 16.0;
  static const double gigantic = 20.0;
}

class FontSize {
  static const double tiny = 8.0;
  static const double small = 10.0;
  static const double medium = 12.0;
  static const double big = 14.0;
  static const double gigantic = 24.0;
}

class Margins {
  static const double tiny = 4;
  static const double small = 8;
  static const double medium = 16;
  static const double big = 32;
  static const double gigantic = 64;
}
class ButtonSize {
  static const double max = 0;
  static const double minimumWidth = 150;
  static const double minimumHeight = 48;
}

