import 'package:flutter/material.dart';

class Colorz {
  static const Color primaryMain = Color(0xFFFF5548);
  static const Color primaryDarkMain = Color(0xAAFF4037);
  static const Color primaryLightMain = Color(0xFFD7CCC8);
  static const Color accentMain = Color(0xFFFFC3C3);
  static const Color layoutBackgroundMain = Color(0xFFFFFFFF);
  static const Color textPrimaryMain = Color(0xFF212121);
  static const Color textSecondaryMain = Color(0xFF757575);
  static const Color dividerMain = Color(0xFFBDBDBD);
  static const Color textWidgetMain = Color(0xFFFFFFFF);
  static const Color disabledMain = Color(0xFF000000);

  static const Color primaryDark = Color(0x992196F3);
  static const Color primaryDarkDark = Color(0x991976D2);
  static const Color primaryLightDark = Color(0xAABBDEFB);
  static const Color accentDark = Color(0xAA009688);
  static const Color layoutBackgroundDark = Color(0xAAFFFFFF);
  static const Color textPrimaryDark = Color(0xAA212121);
  static const Color textSecondaryDark = Color(0xAA757575);
  static const Color dividerDark = Color(0xAA757575);
  static const Color textWidgetDark = Color(0xFFFFFFFF);
  static const Color disabledDark = Color(0xFFD2D2D2);

}

