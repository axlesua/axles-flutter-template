import 'package:flutter/material.dart';
import 'package:some_app/core/config/colorz.dart';
import 'package:some_app/core/config/dimens.dart';

import 'fonts.dart';

enum AppTheme { light, dark }

class Themes {
  static ThemeData get mainTheme {
    return ThemeData(
      brightness: Brightness.light,
      primaryColor: Colorz.primaryMain,
      primaryColorDark: Colorz.primaryDarkMain,
      primaryColorLight: Colorz.primaryLightMain,
      appBarTheme: const AppBarTheme(
        backgroundColor: Colorz.primaryDarkMain,
        centerTitle: false,
        elevation: 15,
        titleTextStyle: TextStyle(
          color: Colorz.textWidgetMain,
        ),
      ),
      buttonTheme: const ButtonThemeData(
        buttonColor: Colorz.accentMain,
        disabledColor: Colorz.disabledMain,
        textTheme: ButtonTextTheme.accent,
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          primary: Colorz.accentMain,
          onSurface: Colorz.primaryDarkMain,
          onPrimary: Colorz.accentMain,
            textStyle: const TextStyle(

            )
        ),
      ),
      fontFamily: Fontz.roboto,
      textTheme: const TextTheme(
        displayLarge: TextStyle(
          fontSize: FontSize.gigantic,
          fontWeight: FontWeight.w900,
        ),
        displayMedium: TextStyle(
          fontSize: FontSize.medium,
          fontWeight: FontWeight.w600,
        ),
        displaySmall: TextStyle(
          fontSize: FontSize.small,
          fontWeight: FontWeight.normal,
        ),
        labelLarge: TextStyle(
          fontSize: FontSize.medium,
          fontWeight: FontWeight.normal,
        ),
      ),
    );
  }

  static ThemeData get darkTheme => mainTheme.copyWith(
        brightness: Brightness.dark,
        primaryColor: Colorz.primaryDark,
        primaryColorDark: Colorz.primaryDarkDark,
        primaryColorLight: Colorz.primaryLightDark,
      );
}

extension CustomTextTheme on TextTheme {
  TextStyle get gigantic => const TextStyle(
        fontSize: FontSize.gigantic,
        fontWeight: FontWeight.w700,
      );
}

extension ExpandedButtonStyle on ElevatedButtonThemeData {
  ButtonStyle get expandedButtonStyle => ButtonStyle(
        minimumSize: MaterialStateProperty.all<Size>(
          const Size.fromHeight(ButtonSize.minimumHeight),
        ), //
      );
}

final appTheme = {
  AppTheme.light: Themes.mainTheme,
  AppTheme.dark: Themes.darkTheme,
};
