class Routes {
  static const String root = '/';
  static const String signIn = '/sign_in';
  static const String signUp = '/signup';
  static const String forgot = '/forgot';

}

class ModuleRoutes {
  static const String auth = '/';
  static const String base = '/base';
}
