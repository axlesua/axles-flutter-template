class Titles {
  static const String appTitle = 'appTitle';
  static const String login = 'Login';
  static const String signUp = 'Sign up';
  static const String username = 'Username';
  static const String email = 'Email';
  static const String password = 'Password';
  static const String confirmPassword = 'Confirm password';
  static const String alreadyAMember = 'Already a member';
}

class UserErrors {
  static const String theInformationProvidedIsIncorrect =
      'The information provided is incorrect, please check and try again';
}
