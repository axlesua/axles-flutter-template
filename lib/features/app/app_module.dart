import 'package:flutter_modular/flutter_modular.dart';
import 'package:some_app/core/constants/routes.dart';
import 'package:some_app/features/auth/auth_module.dart';
import 'package:some_app/features/base/base_module.dart';

class AppModule extends Module {
  @override
  List<ModularRoute> get routes => [
        ModuleRoute(
          ModuleRoutes.auth,
          module: AuthModule(),
        ),
        ModuleRoute(ModuleRoutes.base, module: BaseModule(), guards: []),
      ];
}
