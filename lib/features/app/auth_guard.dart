import 'package:flutter_modular/flutter_modular.dart';
import 'package:some_app/core/common/domain/use_cases/usecase.dart';
import 'package:some_app/core/constants/routes.dart';
import 'package:some_app/features/auth/domain/use_cases/get_user_session.dart';

class AuthGuard extends RouteGuard {
  AuthGuard() : super(redirectTo: ModuleRoutes.auth);

  @override
  Future<bool> canActivate(String path, ModularRoute router) async {
    var userSession = (await Modular.get<GetUserSession>().call(
      NoParams(),
    ))
        .getData();
    return userSession.fold(
      (error) => false,
      (isLoggedIn) => isLoggedIn,
    );
  }
}
