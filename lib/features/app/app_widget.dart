import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:some_app/core/config/themes.dart';

import '../../core/constants/titles.dart';

class AppWidget extends StatefulWidget {
  const AppWidget({Key? key}) : super(key: key);

  @override
  State<AppWidget> createState() => _AppWidgetState();
}

class _AppWidgetState extends State<AppWidget> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      title: Titles.appTitle,
      theme: Themes.mainTheme,
      darkTheme: Themes.darkTheme,
      routeInformationParser: Modular.routeInformationParser,
      routerDelegate: Modular.routerDelegate,
    );
  }
}
