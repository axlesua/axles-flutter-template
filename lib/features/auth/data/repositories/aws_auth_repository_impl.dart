import 'package:dartz/dartz.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:some_app/core/common/data/models/error/response_error.dart';
import 'package:some_app/features/auth/domain/entities/sign_in_result_entity.dart';
import 'package:some_app/features/auth/domain/entities/signup_result_entity.dart';
import 'package:some_app/features/auth/domain/entities/user_data_entity.dart';
import 'package:some_app/features/auth/domain/repositories/auth_repository.dart';
import 'package:some_app/features/auth/domain/use_cases/sign_up.dart';
import 'package:logger/logger.dart';

// class AuthRepositoryImpl implements AuthRepository {
//
//   @override
//   Future<Either<DataFailure, bool>> isUserLoggedIn() async {
//
//   }
//
//   @override
//   Future<Either<DataFailure, UserDataEntity>> getUserData() async {
//     try {
//
//     } on Exception catch (error) {
//
//     }
//   }
//
//   // @override
//   // Future<Either<DataFailure, SignupResultEntity>> signUp({
//   //   required SignUpParams params,
//   // }) async {
//   //   try {
//   //
//   //   } on Exception catch (error) {
//   //
//   //   }
//   // }
//
//   // @override
//   // Future<Either<DataFailure, SignupResultEntity>> confirmSignup({
//   //   required String userName,
//   //   required String confirmationCode,
//   // }) async {
//   //   try {
//   //
//   //   } on Exception catch (error) {
//   //
//   //   }
//   // }
//
//   // @override
//   // Future<Either<DataFailure, SignInResultEntity>> signIn({
//   //   required String userName,
//   //   required String password,
//   // }) async {
//   //   try {
//   //
//   //   } on Exception catch (error) {
//   //
//   //   }
//   // }
//
//   // @override
//   // Future<Either<DataFailure, bool>> signOut() async {
//   //   try {
//   //
//   //   } on Exception catch (error) {
//   //
//   //   }
//   // }
//
//   _mapError(Exception error) {
//     Modular.get<Logger>().e(error);
//     return ResponseError(
//      '$error',//TODO
//       0,
//     );
//   }
// }
