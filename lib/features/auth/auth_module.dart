import 'package:flutter_modular/flutter_modular.dart';
import 'package:some_app/core/constants/routes.dart';
import 'package:some_app/core/core_module.dart';
import 'package:some_app/features/auth/domain/use_cases/confirm_code.dart';
import 'package:some_app/features/auth/domain/use_cases/get_user_session.dart';
import 'package:some_app/features/auth/domain/use_cases/sign_in.dart';
import 'package:some_app/features/auth/presentation/manager/sign_in_bloc.dart';
import 'package:some_app/features/auth/presentation/manager/signup_bloc.dart';
import 'package:some_app/features/auth/presentation/manager/start_bloc.dart';
import 'package:some_app/features/auth/presentation/pages/forgot_password_page.dart';
import 'package:some_app/features/auth/presentation/pages/sign_in_page.dart';
import 'package:some_app/features/auth/presentation/pages/sign_up_page.dart';
import 'package:some_app/features/auth/presentation/pages/start_page.dart';

import 'data/repositories/aws_auth_repository_impl.dart';
import 'domain/use_cases/sign_up.dart';

class AuthModule extends Module {
  @override
  List<Module> get imports => [
        CoreModule(),
      ];

  @override
  List<Bind> get binds => [
        // Bind.lazySingleton((_) => AuthRepositoryImpl()),
        // Bind.lazySingleton((repo) => SignIn(repo())),
        // Bind.lazySingleton((repo) => SignUp(repo())),
        Bind.lazySingleton((repo) => GetUserSession(repo())),
        // Bind.factory((repo) => ConfirmCode(repo())),
        Bind.factory((repo) => StartBloc()),
        Bind.factory((repo) => SignupBloc()),
        Bind.factory((repo) => SignInBloc()),
      ];

  @override
  List<ModularRoute> get routes => [
        ChildRoute(Routes.root, child: (context, args) => const StartPage()),
        ChildRoute(Routes.signIn, child: (context, args) => const SignInPage()),
        ChildRoute(Routes.signUp, child: (context, args) => const SignUpPage()),
        ChildRoute(Routes.forgot,
            child: (context, args) => const ForgotPasswordPage()),
      ];
}
