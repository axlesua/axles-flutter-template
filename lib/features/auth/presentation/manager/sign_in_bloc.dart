import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:some_app/core/utils/extensions/string_extensions.dart';
import 'package:some_app/features/auth/domain/entities/sign_in_result_entity.dart';

import '../../domain/use_cases/sign_in.dart';

part 'sign_in_event.dart';

part 'sign_in_state.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState> {
  // final _signIn = Modular.get<SignIn>();

  SignInBloc() : super(const SignInState()) {
    on<SignInButtonPressedEvent>((event, emit) async {
      await _mapButtonPressedToState(event, emit);
    });

    on<SignInFormChangedEvent>((event, emit) {
      _mapFormChangedToState(event, emit);
    });
  }

  Future<void> _mapButtonPressedToState(
    SignInButtonPressedEvent event,
    Emitter<SignInState> emit,
  ) async {
    // emit(state.copyWith(isProgress: true));

    // var signUpResult = await _signIn(SignInParams(
    //   userName: event.userName,
    //   password: event.password,
    // ));

    // signUpResult.getData().fold((error) {
    //   emit(state.copyWith(
    //     error: error.message,
    //     isProgress: false,
    //   ));
    // }, (signInEntity) {
    //   emit(state.copyWith(
    //     isProgress: false,
    //     entity: signInEntity,
    //   ));
    // });
  }

  void _mapFormChangedToState(
    SignInFormChangedEvent event,
    Emitter<SignInState> emit,
  ) {
    // emit(
    //   // state.copyWith(
    //   //   isButtonEnabled: _validInputFields(event),
    //   // ),
    // );
  }

  bool _validInputFields(SignInFormChangedEvent event) {
    var validEmail = event.userName.isValidUserName();
    var validPassword = event.password.isValidPassword();

    return validEmail & validPassword;
  }
}
