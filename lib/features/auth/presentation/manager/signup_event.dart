part of 'signup_bloc.dart';

abstract class SignupEvent extends Equatable {
  const SignupEvent();
}

class SignUpFormChangedEvent extends SignupEvent {
  final String userName;
  final String email;
  final String password;
  final String passwordConfirmation;

  const SignUpFormChangedEvent({
    required this.userName,
    required this.email,
    required this.password,
    required this.passwordConfirmation,
  });

  @override
  List<Object> get props => [
        userName,
        email,
        password,
        passwordConfirmation,
      ];
}

class SignUpButtonPressedEvent extends SignUpFormChangedEvent {
  const SignUpButtonPressedEvent(
      {required super.userName,
      required super.email,
      required super.password,
      required super.passwordConfirmation});
}
