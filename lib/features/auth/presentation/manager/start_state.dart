part of 'start_bloc.dart';

class StartState extends Equatable {

  final bool _isProgress;
  final bool _isUserLoggedIn;
  final String? _error;

  const StartState(
    this._isProgress,
    this._isUserLoggedIn,
    this._error,
  );

  get isProgress => _isProgress == true;
  get isBottomSheet => !_isProgress && !_isUserLoggedIn;
  get error => _error;
  get navigate => _isUserLoggedIn == true;

  StartState copyWith({
    bool? isProgress,
    bool? isUserLoggedIn,
    String? error,
  }) {
    return StartState(
      isProgress ?? _isProgress,
      isUserLoggedIn ?? _isUserLoggedIn,
      error ?? _error,
    );
  }

  @override
  List<Object?> get props => [
        _isProgress,
        _isUserLoggedIn,
        _error,
      ];
}
