part of 'signup_bloc.dart';

class SignupState extends Equatable {
  final bool isProgress;
  final bool isButtonEnabled;
  final String? error;
  // final SignupResultEntity? entity;

  // bool get navigateNext => entity?.isNextStep == true;

  const SignupState({
    this.isProgress = false,
    this.isButtonEnabled = false,
    this.error,
    // this.entity,
  });

  @override
  List<Object?> get props => [
        isProgress,
        isButtonEnabled,
        error,
        // entity,
      ];

  SignupState copyWith({
    bool? isProgress,
    bool? isButtonEnabled,
    String? error,
    // SignupResultEntity? entity,
  }) {
    return SignupState(
      isProgress: isProgress ?? this.isProgress,
      isButtonEnabled: isButtonEnabled ?? this.isButtonEnabled,
      error: error ?? this.error,
      // entity: entity ?? this.entity,
    );
  }
}
