part of 'forgot_password_bloc.dart';

abstract class ForgotPasswordState extends Equatable{}

class ForgotPasswordInitial extends ForgotPasswordState {

  @override
  List<Object?> get props => [];
}
