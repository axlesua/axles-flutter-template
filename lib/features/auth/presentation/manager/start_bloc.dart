import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:some_app/core/common/domain/use_cases/usecase.dart';
import 'package:some_app/features/auth/domain/use_cases/get_user_session.dart';

part 'start_event.dart';

part 'start_state.dart';

class StartBloc extends Bloc<StartEvent, StartState> {
  final _getUserSession = Modular.get<GetUserSession>();

  StartBloc() : super(const StartState(true, false, null)) {
    on<StartEventCheckUserSession>((event, emit) async {
      emit(state.copyWith(isProgress: true));

      var userSessionData = (await _getUserSession(NoParams())).getData();

      userSessionData.fold((error) {
        emit(state.copyWith(
          isProgress: false,
          error: error.message,
        ));
      }, (isUserLoggedIn) {
        emit(state.copyWith(
          isProgress: false,
          isUserLoggedIn: isUserLoggedIn,
        ));
      });
    });
  }
}
