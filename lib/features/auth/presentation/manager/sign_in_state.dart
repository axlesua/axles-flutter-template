part of 'sign_in_bloc.dart';

class SignInState extends Equatable {
  final bool isProgress;
  final bool isButtonEnabled;
  final String? error;
  // final SignInResultEntity? entity;

  // bool get navigateNext => entity?.isNextStep == true;

  const SignInState({
    this.isProgress = false,
    this.isButtonEnabled = false,
    this.error,
    // this.entity,
  });

  @override
  List<Object?> get props => [
    isProgress,
    isButtonEnabled,
    error,
    // entity,
  ];

  // SignInState copyWith({
  //   bool? isProgress,
  //   bool? isButtonEnabled,
  //   String? error,
  //   SignInResultEntity? entity,
  // }) {
  //   return SignInState(
  //     isProgress: isProgress ?? this.isProgress,
  //     isButtonEnabled: isButtonEnabled ?? this.isButtonEnabled,
  //     error: error ?? this.error,
  //     entity: entity ?? this.entity,
  //   );
  // }
}
