part of 'sign_in_bloc.dart';

abstract class SignInEvent extends Equatable {
  const SignInEvent();
}

class SignInFormChangedEvent extends SignInEvent {
  final String userName;
  final String password;

  const SignInFormChangedEvent({
    required this.userName,
    required this.password,
  });

  @override
  List<Object> get props => [
        userName,
        password,
      ];
}

class SignInButtonPressedEvent extends SignInFormChangedEvent {
  const SignInButtonPressedEvent({
    required super.userName,
    required super.password,
  });
}
