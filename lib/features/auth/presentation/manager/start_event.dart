part of 'start_bloc.dart';

abstract class StartEvent extends Equatable {}

class StartEventCheckUserSession extends StartEvent {

  @override
  List<Object?> get props => [];
}
