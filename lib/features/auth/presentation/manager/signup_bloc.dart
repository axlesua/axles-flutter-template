import 'package:bloc/bloc.dart';
import 'package:collection/collection.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:some_app/core/utils/extensions/string_extensions.dart';
import 'package:some_app/features/auth/domain/entities/signup_result_entity.dart';

import '../../domain/use_cases/sign_up.dart';

part 'signup_event.dart';
part 'signup_state.dart';

class SignupBloc extends Bloc<SignupEvent, SignupState> {

  // final _signUp = Modular.get<SignUp>();

  SignupBloc() : super(const SignupState()) {

    on<SignUpButtonPressedEvent>((event, emit) async {
      await _mapButtonPressedToState(event, emit);
    });

    on<SignUpFormChangedEvent>((event, emit) {
      _mapFormChangedToState(event, emit);
    });
  }

  Future<void> _mapButtonPressedToState(
    SignUpButtonPressedEvent event,
    Emitter<SignupState> emit,
  ) async {
    emit(state.copyWith(
      isProgress: true,
      error: null,
    ));

    // var signUpResult = await _signUp(SignUpParams(
    //   name: event.userName,
    //   email: event.email,
    //   password: event.password,
    // ));

    // signUpResult.getData().fold((error) {
    //   emit(state.copyWith(
    //     error: error.message,
    //     isProgress: false,
    //   ));
    // }, (signupEntity) {
    //   emit(state.copyWith(
    //     isProgress: false,
    //     entity: signupEntity,
    //   ));
    // });
  }

  void _mapFormChangedToState(
    SignUpFormChangedEvent event,
    Emitter<SignupState> emit,
  ) {
    emit(
      state.copyWith(
        isButtonEnabled: _validInputFields(event),
      ),
    );
  }

  bool _validInputFields(SignUpFormChangedEvent event) {
    var validUserName = event.userName.isValidUserName();
    var validEmail = event.email.isValidEmail();
    var validPassword = event.password.isValidPassword();
    var validPasswordConfirmation =
        event.passwordConfirmation.isValidPassword();
    var validPasswordMatch =
        equalsIgnoreAsciiCase(event.password, event.passwordConfirmation);

    return validUserName &
        validEmail &
        validPassword &
        validPasswordConfirmation &
        validPasswordMatch;
  }
}
