import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:some_app/core/constants/titles.dart';

import '../manager/signup_bloc.dart';
import '../widgets/signup_form_widget.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({Key? key}) : super(key: key);

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<SignupBloc>(
      create: (context) => Modular.get<SignupBloc>(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text(Titles.signUp),
        ),
        body: const SignUpFormWidget(),
      ),
    );
  }
}
