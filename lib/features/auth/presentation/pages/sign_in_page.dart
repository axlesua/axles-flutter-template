import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:some_app/core/constants/titles.dart';

import '../manager/sign_in_bloc.dart';
import '../widgets/sign_in_form_widget.dart';
class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<SignInBloc>(
      create: (context) => Modular.get<SignInBloc>(),
      child: Scaffold(
        appBar: AppBar(
          title: Text(Titles.login,),
        ),
        body: const SignInFormWidget(),
      ),
    );
  }
}
