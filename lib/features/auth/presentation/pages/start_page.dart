import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:some_app/features/auth/presentation/manager/start_bloc.dart';
import 'package:some_app/features/auth/presentation/widgets/start_widget.dart';

class StartPage extends StatefulWidget {
  const StartPage({Key? key}) : super(key: key);

  @override
  State<StartPage> createState() => _StartPageState();
}

class _StartPageState extends State<StartPage> {
  @override
  Widget build(BuildContext context) {
    return BlocProvider<StartBloc>(
      create: (context) => Modular.get<StartBloc>()..add(StartEventCheckUserSession()),
      child: const Scaffold(
        body: StartWidget(),
      ),
    );
  }
}
