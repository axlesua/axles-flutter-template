import 'package:flutter/material.dart';

import '../../../../core/config/dimens.dart';

class CustomTextFormField extends StatefulWidget {
  final String label;
  final TextEditingController? controller;
  final FormFieldValidator<String>? validator;
  final TextInputType? textInputType;
  final bool? isPassword;

  const CustomTextFormField({
    Key? key,
    required this.label,
    this.controller,
    this.validator,
    this.textInputType,
    this.isPassword,
  }) : super(key: key);

  @override
  State<CustomTextFormField> createState() => _CustomTextFormFieldState();
}

class _CustomTextFormFieldState extends State<CustomTextFormField> {
  late bool _passwordVisible;

  @override
  void initState() {
    super.initState();
    _passwordVisible = false;
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: obscure,
      obscuringCharacter: '*',
      validator: widget.validator,
      controller: widget.controller,
      keyboardType: widget.textInputType,
      decoration: InputDecoration(
        labelText: widget.label,
        suffixIcon: _passIcon(),
        border: OutlineInputBorder(
          borderRadius: BorderRadius.circular(
            Radiuses.tiny,
          ),
        ),
        filled: true,
      ),
    );
  }

  bool get obscure => (widget.isPassword ?? false) && !_passwordVisible;

  IconButton? _passIcon() {
    if (widget.isPassword == null) return null;
    var icon = Icon(obscure ? Icons.lock : Icons.lock_open);
    return IconButton(
      icon: icon,
      onPressed: () {
        setState(() {
          _passwordVisible = !_passwordVisible;
        });
      },
    );
  }
}
