import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:some_app/core/common/presentation/widgets/empty_widget.dart';
import 'package:some_app/core/constants/routes.dart';
import 'package:some_app/features/auth/presentation/manager/start_bloc.dart';
import 'auth_bottomsheet.dart';

class StartWidget extends StatefulWidget {
  const StartWidget({Key? key}) : super(key: key);

  @override
  State<StartWidget> createState() => _StartWidgetState();
}

class _StartWidgetState extends State<StartWidget>
    with TickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.bottomCenter,
      children: [
        //TODO background
        Center(
          child: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.centerLeft,
                end: Alignment.centerRight,
                colors: [
                  Colors.orange,
                  Colors.red,
                ],
              ),
            ),
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
          ),
        ),
        BlocConsumer<StartBloc, StartState>(
          listener: (context, state) {
            if (state.navigate) {
              Modular.to.navigate(ModuleRoutes.base);
            }
          },
          builder: (context, state) {
            if (state.isProgress) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else if (state.isBottomSheet) {
              return Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  // CustomSlideUpWidget(
                  //   isOpen: true,
                  //   child: AuthBottomSheet(
                  //     onLoginCLick: () {
                  //       Modular.to.pushNamed(
                  //         Routes.signIn,
                  //       );
                  //     },
                  //     onSignUpCLick: () {
                  //       Modular.to.pushNamed(
                  //         Routes.signUp,
                  //       );
                  //     },
                  //   ),
                  // ),
                ],
              );
            } else {
              return const EmptyWidget();
            }
          },
        ),
      ],
    );
  }
}
