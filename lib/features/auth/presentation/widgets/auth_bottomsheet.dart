import 'package:flutter/material.dart';
import 'package:some_app/core/config/dimens.dart';
import 'package:some_app/core/config/themes.dart';
import 'package:some_app/core/constants/titles.dart';
import 'package:some_app/features/auth/presentation/widgets/custom_elevated_button.dart';

class AuthBottomSheet extends StatelessWidget {
  final VoidCallback onLoginCLick;
  final VoidCallback onSignUpCLick;

  const AuthBottomSheet({
    Key? key,
    required this.onLoginCLick,
    required this.onSignUpCLick,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Container(
          padding: const EdgeInsets.all(
            Margins.medium,
          ),
          child: Text(
            Titles.appTitle,
            style: Theme
                .of(context)
                .textTheme
                .gigantic,
          ),
        ),
        CustomElevatedButton(onPressed: onSignUpCLick, text: Titles.signUp,),
        Padding(
          padding: const EdgeInsets.symmetric(vertical: Margins.medium),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Flexible(child: Divider()),
              Text(
                Titles.alreadyAMember,
              ),
              Flexible(child: Divider()),
            ],
          ),
        ),
        CustomElevatedButton(
          onPressed: onLoginCLick,
          text: Titles.login,
        ),
      ],
    );
  }
}
