import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:some_app/core/config/dimens.dart';
import 'package:some_app/core/constants/titles.dart';
import 'package:some_app/features/auth/presentation/manager/signup_bloc.dart';
import 'package:some_app/features/auth/presentation/widgets/custom_elevated_button.dart';
import 'package:some_app/features/auth/presentation/widgets/custom_text_form_field.dart';

class SignUpFormWidget extends StatefulWidget {
  const SignUpFormWidget({Key? key}) : super(key: key);

  @override
  State<SignUpFormWidget> createState() => _SignUpFormWidgetState();
}

class _SignUpFormWidgetState extends State<SignUpFormWidget> {
  late TextEditingController _nameController;
  late TextEditingController _emailController;
  late TextEditingController _passwordController;
  late TextEditingController _passConfirmController;

  @override
  initState() {
    _nameController = TextEditingController()
      ..addListener(() {
        _signUpFormChanged();
      });
    _emailController = TextEditingController()
      ..addListener(() {
        _signUpFormChanged();
      });
    _passwordController = TextEditingController()
      ..addListener(() {
        _signUpFormChanged();
      });
    _passConfirmController = TextEditingController()
      ..addListener(() {
        _signUpFormChanged();
      });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SignupBloc, SignupState>(
      listener: (context, state) {
        if (state.error != null) {
          _showDialogError(state.error!);
        }

        // if (state.navigateNext) {}
      },
      builder: (context, state) {
        return Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [],
        );
      },
    );
  }

  void _showDialogError(String error) async {
    await showDialog(
        context: context,
        builder: (context) {
          return SimpleDialog(children: <Widget>[Center(child: Text(error))]);
        });
  }

  _signUpFormChanged() {
    BlocProvider.of<SignupBloc>(context).add(
      SignUpFormChangedEvent(
        userName: _nameController.text,
        email: _emailController.text,
        password: _passwordController.text,
        passwordConfirmation: _passConfirmController.text,
      ),
    );
  }
}
