import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:some_app/core/config/dimens.dart';
import 'package:some_app/core/constants/titles.dart';
import 'package:some_app/features/auth/presentation/manager/sign_in_bloc.dart';
import 'package:some_app/features/auth/presentation/widgets/custom_elevated_button.dart';
import 'package:some_app/features/auth/presentation/widgets/custom_text_form_field.dart';

class SignInFormWidget extends StatefulWidget {
  const SignInFormWidget({Key? key}) : super(key: key);

  @override
  State<SignInFormWidget> createState() => _SignInFormWidgetState();
}

class _SignInFormWidgetState extends State<SignInFormWidget> {
  late TextEditingController _nameController;
  late TextEditingController _passwordController;

  @override
  initState() {
    _nameController = TextEditingController()
      ..addListener(() {
        _loginFormChanged();
      });

    _passwordController = TextEditingController()
      ..addListener(() {
        _loginFormChanged();
      });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<SignInBloc, SignInState>(
      listener: (context, state) {
        if (state.error != null) {
          _showDialogError(state.error!);
        }

        // if (state.navigateNext) {}
      },
      builder: (context, state) {
        return Column(
          children: [

          ],
        );
      },
    );
  }

  void _showDialogError(String error) async {
    await showDialog(
        context: context,
        builder: (context) {
          return SimpleDialog(children: <Widget>[Center(child: Text(error))]);
        });
  }

  _loginFormChanged() {
    BlocProvider.of<SignInBloc>(context).add(
      SignInFormChangedEvent(
        userName: _nameController.text,
        password: _passwordController.text,
      ),
    );
  }
}
