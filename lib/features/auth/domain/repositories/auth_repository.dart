import 'package:dartz/dartz.dart';
import 'package:some_app/features/auth/domain/use_cases/sign_up.dart';

import '../../../../core/common/data/models/error/response_error.dart';
import '../entities/sign_in_result_entity.dart';
import '../entities/signup_result_entity.dart';
import '../entities/user_data_entity.dart';

abstract class AuthRepository {
  Future<Either<DataFailure, bool>> isUserLoggedIn();

  // Future<Either<DataFailure, UserDataEntity>> getUserData();
  //
  // Future<Either<DataFailure, SignupResultEntity>> signUp({
  //   required SignUpParams params,
  // });
  //
  // Future<Either<DataFailure, SignupResultEntity>> confirmSignup({
  //   required String userName,
  //   required String confirmationCode,
  // });

  // Future<Either<DataFailure, SignInResultEntity>> signIn({
  //   required String userName,
  //   required String password,
  // });

  Future<Either<DataFailure, bool>> signOut();
}
