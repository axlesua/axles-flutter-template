import 'package:freezed_annotation/freezed_annotation.dart';

part 'user_session_entity.freezed.dart';

@freezed
class UserSessionEntity with _$UserSessionEntity {
  const UserSessionEntity._();
  //
  // const factory UserSessionEntity({
  //   required UserModel userModel,
  // }) = _UserSessionEntity;
}
