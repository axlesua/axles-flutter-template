// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'user_session_entity.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more informations: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
class _$UserSessionEntityTearOff {
  const _$UserSessionEntityTearOff();

  _UserSessionEntity call({required UserModel UserModel}) {
    return _UserSessionEntity(
      UserModel: UserModel,
    );
  }
}

/// @nodoc
const $UserSessionEntity = _$UserSessionEntityTearOff();

/// @nodoc
mixin _$UserSessionEntity {
  UserModel get UserModel => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $UserSessionEntityCopyWith<UserSessionEntity> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UserSessionEntityCopyWith<$Res> {
  factory $UserSessionEntityCopyWith(
          UserSessionEntity value, $Res Function(UserSessionEntity) then) =
      _$UserSessionEntityCopyWithImpl<$Res>;
  $Res call({UserModel UserModel});

  $UserModelCopyWith<$Res> get UserModel;
}

/// @nodoc
class _$UserSessionEntityCopyWithImpl<$Res>
    implements $UserSessionEntityCopyWith<$Res> {
  _$UserSessionEntityCopyWithImpl(this._value, this._then);

  final UserSessionEntity _value;
  // ignore: unused_field
  final $Res Function(UserSessionEntity) _then;

  @override
  $Res call({
    Object? UserModel = freezed,
  }) {
    return _then(_value.copyWith(
      UserModel: UserModel == freezed
          ? _value.awsUserModel
          : awsUserModel // ignore: cast_nullable_to_non_nullable
              as UserModel,
    ));
  }

  @override
  $UserModelCopyWith<$Res> get awsUserModel {
    return $UserModelCopyWith<$Res>(_value.awsUserModel, (value) {
      return _then(_value.copyWith(awsUserModel: value));
    });
  }
}

/// @nodoc
abstract class _$UserSessionEntityCopyWith<$Res>
    implements $UserSessionEntityCopyWith<$Res> {
  factory _$UserSessionEntityCopyWith(
          _UserSessionEntity value, $Res Function(_UserSessionEntity) then) =
      __$UserSessionEntityCopyWithImpl<$Res>;
  @override
  $Res call({UserModel awsUserModel});

  @override
  $UserModelCopyWith<$Res> get awsUserModel;
}

/// @nodoc
class __$UserSessionEntityCopyWithImpl<$Res>
    extends _$UserSessionEntityCopyWithImpl<$Res>
    implements _$UserSessionEntityCopyWith<$Res> {
  __$UserSessionEntityCopyWithImpl(
      _UserSessionEntity _value, $Res Function(_UserSessionEntity) _then)
      : super(_value, (v) => _then(v as _UserSessionEntity));

  @override
  _UserSessionEntity get _value => super._value as _UserSessionEntity;

  @override
  $Res call({
    Object? awsUserModel = freezed,
  }) {
    return _then(_UserSessionEntity(
      awsUserModel: awsUserModel == freezed
          ? _value.awsUserModel
          : awsUserModel // ignore: cast_nullable_to_non_nullable
              as UserModel,
    ));
  }
}

/// @nodoc

class _$_UserSessionEntity extends _UserSessionEntity {
  const _$_UserSessionEntity({required this.awsUserModel}) : super._();

  @override
  final UserModel awsUserModel;

  @override
  String toString() {
    return 'UserSessionEntity(awsUserModel: $awsUserModel)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _UserSessionEntity &&
            const DeepCollectionEquality()
                .equals(other.awsUserModel, awsUserModel));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(awsUserModel));

  @JsonKey(ignore: true)
  @override
  _$UserSessionEntityCopyWith<_UserSessionEntity> get copyWith =>
      __$UserSessionEntityCopyWithImpl<_UserSessionEntity>(this, _$identity);
}

abstract class _UserSessionEntity extends UserSessionEntity {
  const factory _UserSessionEntity({required UserModel awsUserModel}) =
      _$_UserSessionEntity;
  const _UserSessionEntity._() : super._();

  @override
  UserModel get awsUserModel;
  @override
  @JsonKey(ignore: true)
  _$UserSessionEntityCopyWith<_UserSessionEntity> get copyWith =>
      throw _privateConstructorUsedError;
}
