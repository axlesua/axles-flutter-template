import 'package:freezed_annotation/freezed_annotation.dart';

// part 'sign_in_result_entity.freezed.dart';
//
// @freezed
// class SignInResultEntity with _$SignInResultEntity {
//
//   const SignInResultEntity._();
//
//   const factory SignInResultEntity({
//     required SignInResult signInResult,
//   }) = _SignInResultEntity;
//
//   bool get isSignedIn => signInResult.isSignedIn;
//   bool get isNextStep => signInResult.nextStep != null;
// }
