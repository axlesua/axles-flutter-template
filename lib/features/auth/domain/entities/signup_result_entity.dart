// import 'package:amplify_auth_cognito/amplify_auth_cognito.dart';
import 'package:flutter/foundation.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

// part 'signup_result_entity.freezed.dart';

// @freezed
// class SignupResultEntity with _$SignupResultEntity {
//   const SignupResultEntity._();
//
//   const factory SignupResultEntity({
//     required SignUpResult signUpResult,
//   }) = _SignupResultEntity;
//
//   bool get isSignUpComplete => signUpResult.isSignUpComplete;
//   bool get isNextStep => signUpResult.nextStep.signUpStep == 'CONFIRM_SIGN_UP_STEP';
// }
