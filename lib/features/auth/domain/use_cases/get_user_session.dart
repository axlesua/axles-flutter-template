import 'package:some_app/core/common/domain/entities/result.dart';
import 'package:some_app/core/common/domain/use_cases/usecase.dart';

import '../repositories/auth_repository.dart';

class GetUserSession extends UseCase<bool, NoParams> {
  final AuthRepository _authRepository;

  GetUserSession(this._authRepository);

  @override
  Future<Result<bool>> call(NoParams params) async {
    return Result(await _authRepository.isUserLoggedIn());
  }
}
