import 'package:equatable/equatable.dart';
import 'package:some_app/features/auth/domain/repositories/auth_repository.dart';

import '../../../../core/common/domain/entities/result.dart';
import '../../../../core/common/domain/use_cases/usecase.dart';
import '../entities/signup_result_entity.dart';

// class SignUp extends UseCase<SignupResultEntity, SignUpParams> {
//   final AuthRepository _authRepository;
//
//   SignUp(this._authRepository);
//
//   // @override
//   // Future<Result<SignupResultEntity>> call(SignUpParams params) async {
//   //   return Result(await _authRepository.signUp(
//   //     params: params
//   //   ));
//   // }
// }

class SignUpParams extends Equatable {
  final String name;
  final String email;
  final String password;

  const SignUpParams({
    required this.name,
    required this.email,
    required this.password,
  });

  @override
  List<Object?> get props => [];
}
