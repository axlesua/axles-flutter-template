import 'package:equatable/equatable.dart';
import 'package:some_app/core/common/domain/entities/result.dart';
import 'package:some_app/core/common/domain/use_cases/usecase.dart';
import 'package:some_app/features/auth/domain/repositories/auth_repository.dart';

import '../entities/sign_in_result_entity.dart';

// class SignIn extends UseCase<SignInResultEntity, SignInParams> {
//   final AuthRepository _authRepository;
//
//   SignIn(this._authRepository);
//
//   @override
//   Future<Result<SignInResultEntity>> call(SignInParams params) async {
//
//     var signInResult = await _authRepository.signIn(
//       userName: params.userName,
//       password: params.password,
//     );
//
//     return Result(signInResult);
//   }
// }

class SignInParams extends Equatable {
  final String userName;
  final String password;

  const SignInParams({required this.userName, required this.password});

  @override
  List<Object?> get props => [
        userName,
        password,
      ];
}
