import 'package:equatable/equatable.dart';

import '../../../../core/common/domain/entities/result.dart';
import '../../../../core/common/domain/use_cases/usecase.dart';
import '../entities/signup_result_entity.dart';
import '../repositories/auth_repository.dart';
//
// class ConfirmCode extends UseCase<SignupResultEntity, ConfirmCodeParams> {
//   final AuthRepository _authRepository;
//
//   ConfirmCode(this._authRepository);
//
//   @override
//   Future<Result<SignupResultEntity>> call(ConfirmCodeParams params) async {
//     return Result(await _authRepository.confirmSignup(
//       userName: params.userName,
//       confirmationCode: params.code,
//     ));
//   }
// }

class ConfirmCodeParams extends Equatable {
  final String userName;
  final String code;

  const ConfirmCodeParams({required this.userName, required this.code});

  @override
  List<Object?> get props => [
        userName,
        code,
      ];
}
