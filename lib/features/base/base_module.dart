import 'package:flutter_modular/flutter_modular.dart';
import 'package:some_app/core/constants/routes.dart';
import 'package:some_app/features/base/presentation/pages/base_page.dart';

class BaseModule extends Module {

  @override
  List<Bind<Object>> get binds => [];

  @override
  List<ModularRoute> get routes => [
        ChildRoute(Routes.root, child: (_, __) => const BasePage()),
      ];
}
