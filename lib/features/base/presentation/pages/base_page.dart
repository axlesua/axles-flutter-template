import 'package:flutter/material.dart';
import 'package:some_app/features/base/presentation/widgets/base_widget.dart';

class BasePage extends StatefulWidget {
  const BasePage({Key? key}) : super(key: key);

  @override
  State<BasePage> createState() => _BasePageState();
}

class _BasePageState extends State<BasePage>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);

    return const BaseWidget();
  }

  @override
  bool get wantKeepAlive => true;
}
