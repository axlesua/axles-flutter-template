import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:some_app/core/constants/routes.dart';
import 'package:some_app/core/constants/urls.dart';
import 'core/config/build_environment.dart';
import 'features/app/app_module.dart';
import 'features/app/app_widget.dart';

void main() async {

  BuildEnvironment.init(
    flavor: BuildFlavor.production,
    baseUrl: Urls.base,
  ); //MOCK
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]).then((_) {
    Modular.setInitialRoute(Routes.root);
    runApp(ModularApp(
      module: AppModule(),
      child: const AppWidget(),
    ),);
  });

}
